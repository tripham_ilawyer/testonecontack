package fl.pct.com.testonecontack.model;

import org.xmlpull.v1.XmlPullParser;

/**
 * Created by Tri Pham on 12/28/2016.
 */

public class ResponseEmployeesNode extends ResponseEmployeesVisitor {
    public static final String ATTR_NAME = "employees";
    public ResponseEmployeeIdNode employeeIdNode;

    public ResponseEmployeesNode() {
        attributeName = ATTR_NAME;
    }

    @Override
    public void visitEmployeeId(XmlPullParser parser) {
        employeeIdNode = new ResponseEmployeeIdNode();
        if (parser.getName().equals(ResponseEmployeeIdNode.ATTR_NAME)) {
            employeeIdNode.visitAttribute(parser);
        }
    }
}