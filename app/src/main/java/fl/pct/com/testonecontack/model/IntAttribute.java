package fl.pct.com.testonecontack.model;

/**
 * Created by Tri Pham on 12/29/2016.
 */

public class IntAttribute extends Attribute {
    public int value;

    public IntAttribute() {
        this.type = INT_TYPE;
    }

    public IntAttribute(int value) {
        this.type = INT_TYPE;
        this.value = value;
    }

    public void setInt(int value) {
        this.value = value;
    }
}
