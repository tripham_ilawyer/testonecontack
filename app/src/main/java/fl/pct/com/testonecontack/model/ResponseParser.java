package fl.pct.com.testonecontack.model;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import fl.pct.com.testonecontack.util.XmlUtil;

/**
 * Created by Tri Pham on 12/28/2016.
 */

public class ResponseParser {
    private XmlPullParser parser;
    public ResponseMessageNode messageNode;

    public ResponseParser() {
        parser = Xml.newPullParser();
    }

    public void readMessage(String xmlResponse) {
        InputStream in = new ByteArrayInputStream(xmlResponse.getBytes(Charset.forName("UTF-8")));
        try {
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        messageNode = new ResponseMessageNode();
        try {
            while (parser.next() != XmlPullParser.END_TAG || !parser.getName().equals(ResponseMessageNode.ATTR_NAME)) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                String name = parser.getName();

                // TODO: Add parsing other attribute within "<message></message>" here orderly

                // Starts by looking for the entry tag
                if (name.equals(ResponseUserNode.ATTR_NAME)) {
                    messageNode.visitUser(parser);
                } else if (name.equals(ResponseBodyNode.ATTR_NAME)) {
                    messageNode.visitBody(parser);
                } else {
                    XmlUtil.skip(parser);
                }
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getCommand() {
        if (messageNode != null && messageNode.userNode != null && messageNode.userNode.commandNode != null && messageNode.userNode.commandNode.attributeVal != null) {
            return messageNode.userNode.commandNode.attributeVal.value;
        }
        return "";
    }

    public boolean getResult() {
        if (messageNode != null && messageNode.bodyNode != null && messageNode.bodyNode.resultNode != null && messageNode.bodyNode.resultNode.attributeVal != null) {
            int val = messageNode.bodyNode.resultNode.attributeVal.value;
            return (val == 1);
        }
        return false;
    }

    public String getEmployeeId() {
        if (messageNode != null && messageNode.bodyNode != null && messageNode.bodyNode.employeesNode != null
                && messageNode.bodyNode.employeesNode.employeeIdNode != null && messageNode.bodyNode.employeesNode.employeeIdNode.attributeVal != null) {
            String val = messageNode.bodyNode.employeesNode.employeeIdNode.attributeVal.value;
            return val;
        }
        return "";
    }
}
