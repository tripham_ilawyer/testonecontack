package fl.pct.com.testonecontack.service;

/**
 * Created by Tri Pham on 12/25/2016.
 */

public interface OneContackService {

    boolean performConnect();

    boolean performLogin();

    boolean performCall(String numToCall);

    void setOnConnectedListener(OnConnectedListener onConnectedListener);

    void setOnLoginListener(OnLoginListener onLoginListener);

    void setOnCallListener(OnCallListener onCallListener);


    interface OnConnectedListener {
        void onConnectCompleted(boolean success, Exception errException);
    }

    interface OnLoginListener {
        void onLoginCompleted(boolean success, String commandResp, String employeeId);
    }

    interface OnCallListener {
        void onCallCompleted(boolean success, String commandResp);
    }
}
