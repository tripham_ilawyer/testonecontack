package fl.pct.com.testonecontack.model;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import fl.pct.com.testonecontack.util.XmlUtil;

/**
 * Created by Tri Pham on 12/29/2016.
 */

public class ResponseSerialIdNode extends ResponseSerialIdVisitor {
    public static final String ATTR_NAME = "serialid";
    public IntAttribute attributeVal;

    @Override
    public void visitAttribute(XmlPullParser parser) {
        attributeVal = new IntAttribute();
        try {
            parser.require(XmlPullParser.START_TAG, null, ATTR_NAME);
            String serialId = XmlUtil.readText(parser);
            parser.require(XmlPullParser.END_TAG, null, ATTR_NAME);
            attributeVal.setInt(Integer.valueOf(serialId));
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
}
