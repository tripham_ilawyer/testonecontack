package fl.pct.com.testonecontack.service;

import android.util.Xml;

import com.orhanobut.logger.Logger;

import org.xmlpull.v1.XmlSerializer;

import java.io.StringWriter;
import java.util.Random;

import fl.pct.com.testonecontack.model.ResponseParser;


/**
 * Created by Tri Pham on 12/25/2016.
 */

public class OneContackServiceImpl implements OneContackService, SocketClient.OnConnectCompletedCallbackListener, SocketClient.OnDataCallbackListener {
    public static final String MOBILE_CODE = "0901801101";
    public static final String PASSWORD = "111111";
    public static final String EMPLOYEE_NO = "103";
    private static final String USER_TYPE = "pda";
    private static final String COMMAND_LOGIN_REQ = "OALoginReq";
    private static final String COMMAND_LOGIN_RESP = "OALoginResp";
    private static final String COMMAND_CALL_REQ = "XNEmployeeCallOutReq";
    private static final String COMMAND_CALL_RESP = "PubAppCallResp";
    private static final String BUSINESS = "XN";
    private static final int BODY_LOGIN_TYPE = 1;
    private static final int BODY_CALLED_TYPE = 7;
    private static final int BODY_CALLER_PARAM_TYPE = 1;

    private String employeeId = null;

    private SocketClient socketClient;
    private OnConnectedListener connectedListener;
    private OnLoginListener loginListener;
    private OnCallListener callListener;
    private Random random;

    public OneContackServiceImpl(OnConnectedListener connectedListener) {
        this.random = new Random();
        this.connectedListener = connectedListener;
        socketClient = SocketClient.getInstance();
        socketClient.setOnConnectCompletedCallbackListener(this);
        socketClient.setOnDataCallbackListener(this);
    }

    @Override
    public void setOnConnectedListener(OnConnectedListener connectedListener) {
        this.connectedListener = connectedListener;
    }

    @Override
    public void setOnLoginListener(OnLoginListener loginListener) {
        this.loginListener = loginListener;
    }

    @Override
    public void setOnCallListener(OnCallListener callListener) {
        this.callListener = callListener;
    }

    @Override
    public boolean performConnect() {
        socketClient.connect();
        return true;
    }

    @Override
    public boolean performLogin() {
        String xmlLogin = buildXMLLogin(MOBILE_CODE, PASSWORD, EMPLOYEE_NO);
        return socketClient.writeString(xmlLogin);
    }

    @Override
    public boolean performCall(String numToCall) {
        String xmlCallReq = buildXMLCallReq(MOBILE_CODE, employeeId, numToCall);
        return socketClient.writeString(xmlCallReq);
    }

    @Override
    public void onConnectCompletedCallback(boolean success, Exception errException) {
        if (connectedListener != null) {
            connectedListener.onConnectCompleted(success, errException);
        }
    }

    private String buildXMLLogin(String mobileCode, String password, String employeeNo) {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            serializer.setOutput(writer);
            serializer.startTag("", "message");

            serializer.startTag("", "user");

            serializer.startTag("", "serialid");
            serializer.text(Integer.toString(random.nextInt()));
            serializer.endTag("", "serialid");

            serializer.startTag("", "mobilecode");
            serializer.text(mobileCode);
            serializer.endTag("", "mobilecode");

            serializer.startTag("", "type");
            serializer.text(USER_TYPE);
            serializer.endTag("", "type");

            serializer.startTag("", "command");
            serializer.text(COMMAND_LOGIN_REQ);
            serializer.endTag("", "command");

            serializer.startTag("", "business");
            serializer.text(BUSINESS);
            serializer.endTag("", "business");

            serializer.endTag("", "user");

            serializer.startTag("", "body");

            serializer.startTag("", "type");
            serializer.text(Integer.toString(BODY_LOGIN_TYPE));
            serializer.endTag("", "type");

            serializer.startTag("", "password");
            serializer.text(password);
            serializer.endTag("", "password");

            serializer.startTag("", "employeeno");
            serializer.text(employeeNo);
            serializer.endTag("", "employeeno");

            serializer.endTag("", "body");

            serializer.endTag("", "message");
            serializer.endDocument();
            String s = writer.toString();

            Logger.d(s);
            return s;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    private String buildXMLCallReq(String mobileCode, String callerId, String numberToCall) {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            serializer.setOutput(writer);
            serializer.startTag("", "message");

            serializer.startTag("", "user");

            serializer.startTag("", "serialid");
            serializer.text(Integer.toString(random.nextInt()));
            serializer.endTag("", "serialid");

            serializer.startTag("", "mobilecode");
            serializer.text(mobileCode);
            serializer.endTag("", "mobilecode");

            serializer.startTag("", "type");
            serializer.text(USER_TYPE);
            serializer.endTag("", "type");

            serializer.startTag("", "command");
            serializer.text(COMMAND_CALL_REQ);
            serializer.endTag("", "command");

            serializer.startTag("", "business");
            serializer.text(BUSINESS);
            serializer.endTag("", "business");

            serializer.endTag("", "user");

            serializer.startTag("", "body");

            serializer.startTag("", "calledType");
            serializer.text(Integer.toString(BODY_CALLED_TYPE));
            serializer.endTag("", "calledType");

            serializer.startTag("", "callerID");
            serializer.text(callerId);
            serializer.endTag("", "callerID");

            serializer.startTag("", "callerParamType");
            serializer.text(Integer.toString(BODY_CALLER_PARAM_TYPE));
            serializer.endTag("", "callerParamType");

            serializer.startTag("", "calledParam");
            serializer.text(numberToCall);
            serializer.endTag("", "calledParam");

            serializer.endTag("", "body");

            serializer.endTag("", "message");
            serializer.endDocument();
            String s = writer.toString();

            Logger.d(s);
            return s;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    private void handleLoginResponse(ResponseParser parser) {
        if (parser != null && loginListener != null) {
            employeeId = parser.getEmployeeId();
            loginListener.onLoginCompleted(parser.getResult(), parser.getCommand(), employeeId);
        }
    }

    private void handleCallResponse(ResponseParser parser) {
        if (parser != null && callListener != null) {
            callListener.onCallCompleted(parser.getResult(), parser.getCommand());
        }
    }

    @Override
    public void onDataCallBack(String xmlStr) {
        ResponseParser parser = new ResponseParser();
        parser.readMessage(xmlStr);
        String command = parser.getCommand();
        Logger.d("Command type: " + command);

        if (command.equals(COMMAND_LOGIN_RESP)) {
            handleLoginResponse(parser);
        } else if (command.equals(COMMAND_LOGIN_RESP)) {
            handleCallResponse(parser);
        }
    }
}
