package fl.pct.com.testonecontack.model;

import org.xmlpull.v1.XmlPullParser;

/**
 * Created by Tri Pham on 12/29/2016.
 */

public class ResponseCommandVisitor extends ResponseNode {
    protected ResponseCommandVisitor rcv;

    public void visitAttribute(XmlPullParser parser) {
        if (rcv != null) {
            rcv.visitAttribute(parser);
        }
    }
}
