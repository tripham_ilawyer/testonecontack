package fl.pct.com.testonecontack.model;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import fl.pct.com.testonecontack.util.XmlUtil;

/**
 * Created by Tri Pham on 12/29/2016.
 */

public class ResponseCommandNode extends ResponseCommandVisitor {
    public static final String ATTR_NAME = "command";
    public TextAttribute attributeVal;

    public ResponseCommandNode() {
        attributeName = ATTR_NAME;
    }

    public ResponseCommandNode(String text) {
        attributeName = ATTR_NAME;
        this.attributeVal = new TextAttribute(text);
    }

    @Override
    public void visitAttribute(XmlPullParser parser) {
        attributeVal = new TextAttribute();
        try {
            parser.require(XmlPullParser.START_TAG, null, ATTR_NAME);
            String command = XmlUtil.readText(parser);
            parser.require(XmlPullParser.END_TAG, null, ATTR_NAME);
            attributeVal.setText(command);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
