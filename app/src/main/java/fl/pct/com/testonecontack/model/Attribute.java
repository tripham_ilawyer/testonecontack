package fl.pct.com.testonecontack.model;

/**
 * Created by Tri Pham on 12/28/2016.
 */

public abstract class Attribute {

    public static final int STRING_TYPE = 1;
    public static final int INT_TYPE = 2;

    public int type;
}
