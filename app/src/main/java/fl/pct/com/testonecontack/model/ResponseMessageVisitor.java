package fl.pct.com.testonecontack.model;

import org.xmlpull.v1.XmlPullParser;

/**
 * Created by Tri Pham on 12/28/2016.
 */

public abstract class ResponseMessageVisitor extends ResponseNode {
    protected ResponseMessageVisitor rmv;

    public ResponseUserNode userNode;

    public ResponseBodyNode bodyNode;

    public void visitUser(XmlPullParser parser) {
        if (rmv != null) {
            rmv.visitUser(parser);
        }
    }

    public void visitBody(XmlPullParser parser) {
        if (rmv != null) {
            rmv.visitBody(parser);
        }
    }
}

