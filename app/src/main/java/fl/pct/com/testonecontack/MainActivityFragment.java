package fl.pct.com.testonecontack;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import fl.pct.com.testonecontack.service.OneContackService;
import fl.pct.com.testonecontack.service.OneContackServiceImpl;
import fl.pct.com.testonecontack.model.ResponseParser;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements OneContackService.OnConnectedListener, OneContackService.OnLoginListener, OneContackService.OnCallListener {
    private EditText edtResult;

    private OneContackService oneContackService;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button btnConnect = (Button) view.findViewById(R.id.connect);
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performConnect();
            }
        });

        Button btnLogIn = (Button) view.findViewById(R.id.log_in);
        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performLogin();
            }
        });

        final EditText edtPhoneNum = (EditText) view.findViewById(R.id.phone_num);
        Button btnCall = (Button) view.findViewById(R.id.calling);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performCall(edtPhoneNum.getText().toString());
            }
        });

        edtResult = (EditText) view.findViewById(R.id.result);
    }

    private void performConnect() {
        edtResult.setText("Connecting ...");
        oneContackService = new OneContackServiceImpl(this);
    }

    private void performLogin() {
        oneContackService.setOnLoginListener(this);
        oneContackService.performLogin();
    }

    private void performCall(String numToCall) {
        oneContackService.setOnCallListener(this);
        oneContackService.performCall(numToCall);
    }

    @Override
    public void onConnectCompleted(final boolean success, final Exception errException) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (success) {
                    edtResult.setText("Connect result -> SUCCESS");
                } else {
                    edtResult.setText("Connect result -> " + "FAILED with exception: " + errException.getMessage());
                }
            }
        });
    }

    @Override
    public void onLoginCompleted(final boolean success, final String commandResp, final String employeeId) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                edtResult.setText("Login result: " + success + "\n"
                    + "Command response: " + commandResp + "\n"
                    + "Employee id: " + employeeId + "\n");
            }
        });
    }

    @Override
    public void onCallCompleted(final boolean success, final String commandResp) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                edtResult.setText("Call result: " + success + "\n"
                        + "Command response: " + commandResp);
            }
        });
    }
}
