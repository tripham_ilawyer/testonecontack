package fl.pct.com.testonecontack.model;

import org.xmlpull.v1.XmlPullParser;

/**
 * Created by Tri Pham on 12/28/2016.
 */

public abstract class ResponseEmployeesVisitor extends ResponseNode{
    protected ResponseEmployeesVisitor rlev;

    public void visitEmployeeId(XmlPullParser parser){
        if (rlev != null) {
            rlev.visitEmployeeId(parser);
        }
    }
}
