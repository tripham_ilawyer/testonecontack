package fl.pct.com.testonecontack.model;

import org.xmlpull.v1.XmlPullParser;

/**
 * Created by Tri Pham on 12/29/2016.
 */

public abstract class ResponseResultVisitor extends ResponseNode {
    protected ResponseResultVisitor rrv;

    public void visitAttribute(XmlPullParser parser) {
        if (rrv != null) {
            rrv.visitAttribute(parser);
        }
    }
}
