package fl.pct.com.testonecontack.model;

/**
 * Created by Tri Pham on 12/28/2016.
 */

public class TextAttribute extends Attribute {

    public String value;

    public TextAttribute() {
        this.type = STRING_TYPE;
    }

    public TextAttribute(String value) {
        this.type = STRING_TYPE;
        this.value = value;
    }

    public void setText(String value) {
        this.value = value;
    }
}
