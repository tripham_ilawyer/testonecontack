package fl.pct.com.testonecontack.util;

import com.alibaba.fastjson.JSON;

/**
 * Created by Tri Pham on 12/25/2016.
 */

public class FastJsonUtil {

    public static String createJson(Object obj) {
        return JSON.toJSONString(obj);
    }

    public static <T>T parseJson(String jsonStr, Class<T> cls) {
        return JSON.parseObject(jsonStr, cls);
    }
}
