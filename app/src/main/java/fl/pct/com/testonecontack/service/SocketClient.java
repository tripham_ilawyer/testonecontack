package fl.pct.com.testonecontack.service;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.koushikdutta.async.AsyncServer;
import com.koushikdutta.async.AsyncSocket;
import com.koushikdutta.async.ByteBufferList;
import com.koushikdutta.async.DataEmitter;
import com.koushikdutta.async.callback.CompletedCallback;
import com.koushikdutta.async.callback.ConnectCallback;
import com.koushikdutta.async.callback.DataCallback;
import com.orhanobut.logger.Logger;


import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Random;

import fl.pct.com.testonecontack.util.Hex;
import fl.pct.com.testonecontack.util.ToastTip;

/**
 * Created by Tri Pham on 12/25/2016.
 */

public class SocketClient implements Runnable {

    private String TAG = "log-" + this.getClass().getSimpleName();
    private static final int CONNECT_SUCCESS = 0;
    private static final int CONNECT_FAILED = 1;

    private AsyncSocket asyncSocket = null;
    private String host;
    private int port;
    private static SocketClient single = null;

    private final int ONE_SECOND = 1000;
    private final int TIME_CONNECT_FAILED_DELAY = 5;
    private final int TIME_CONNECT_SUCCESS_DELAY = 30;

    private int reConnectCount = 0;
    private boolean isConnected = false;
    private StringBuffer responseSb = new StringBuffer();
    private int packetSize = 0;

    private OnConnectCompletedCallbackListener onConnectCompletedCallbackListener;
    private OnDataCallbackListener onDataCallbackListener;
    private OnCloseCallbackListener onCloseCallbackListener;
    private OnEndCallbackListener onEndCallbackListener;

    public interface OnConnectCompletedCallbackListener {
        void onConnectCompletedCallback(boolean success, Exception errException);
    }

    public interface OnDataCallbackListener {
        void onDataCallBack(String xmlStr);
    }

    public interface OnCloseCallbackListener {
        void onCloseCallBack();
    }

    public interface OnEndCallbackListener {
        void onEndCallBack();
    }

    public void setOnConnectCompletedCallbackListener(OnConnectCompletedCallbackListener onConnectCompletedCallbackListener) {
        this.onConnectCompletedCallbackListener = onConnectCompletedCallbackListener;
    }

    public void setOnDataCallbackListener(OnDataCallbackListener onDataCallbackListener) {
        this.onDataCallbackListener = onDataCallbackListener;
    }

    public void setOnCloseCallbackListener(OnCloseCallbackListener onCloseCallbackListener) {
        this.onCloseCallbackListener = onCloseCallbackListener;
    }

    public void setOnEndCallbackListener(OnEndCallbackListener onEndCallbackListener) {
        this.onEndCallbackListener = onEndCallbackListener;
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case CONNECT_SUCCESS:
                    break;
                case CONNECT_FAILED:
                    closeSocket();
                    connect();
                    break;
            }
        }
    };

    private SocketClient(String host, int port) {
        this.host = host;
        this.port = port;
        mHandler.postDelayed(this, ONE_SECOND);
    }

    public static SocketClient getInstance() {
        if (single == null) {
            single = new SocketClient(ConstantParams.IP, ConstantParams.PORT);
        }
        return single;
    }

    @Override
    public void run() {
        mHandler.postDelayed(this, ONE_SECOND);
        reConnectCount++;
        if (isConnected) {
            if (reConnectCount > TIME_CONNECT_SUCCESS_DELAY) {
                reConnectCount = 0;
                sendMessage(CONNECT_SUCCESS);
            }
        } else {
            if (reConnectCount % TIME_CONNECT_FAILED_DELAY == 0) {
                sendMessage(CONNECT_FAILED);
            }
        }
    }

    private void sendMessage(int what) {
        Message msg = mHandler.obtainMessage();
        msg.what = what;
        mHandler.sendMessage(msg);
    }

    /**
     * connect Socket
     */
    public void connect() {
        AsyncServer.getDefault().connectSocket(new InetSocketAddress(host, port), new ConnectCallback() {
            @Override
            public void onConnectCompleted(Exception ex, final AsyncSocket socket) {
                if (ex != null) {
                    if (onConnectCompletedCallbackListener != null) {
                        onConnectCompletedCallbackListener.onConnectCompletedCallback(false, ex);
                    }
                    Log.e(TAG, "AsyncSocket Exception: " + ex.getMessage());
                    return;
                }
                if (socket == null) {
                    closeSocket();
                    connect();
                    return;
                }
                asyncSocket = socket;
                isConnected = true;
                Log.d(TAG, "---> Connected");

                onConnectCompletedCallBack(socket);

                if (onConnectCompletedCallbackListener != null) {
                    onConnectCompletedCallbackListener.onConnectCompletedCallback(true, null);
                }
            }
        });
    }

    public void onConnectCompletedCallBack(final AsyncSocket socket) {
        socket.setDataCallback(new DataCallback() {
            @Override
            public void onDataAvailable(DataEmitter emitter, ByteBufferList bb) {
                if (bb == null) {
                    return;
                }
                Logger.d("isChunked: " + emitter.isChunked() + ", remaining: " + bb.remaining());
                DataInputStream dis = new DataInputStream(new ByteArrayInputStream(bb.getAllByteArray()));
                if (dis == null) {
                    return;
                }
                try {
                    String xmlStr = read(dis);
                    dis.close();
                    Log.d(TAG, "Receive : " + xmlStr);

                    if (TextUtils.isEmpty(xmlStr)) {
                        return;
                    }
                    // Return if we're processing non-complete multi-part packet
                    if (packetSize != 0) {
                        return;
                    }
                    if (onDataCallbackListener != null) {
                        onDataCallbackListener.onDataCallBack(xmlStr);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        socket.setClosedCallback(new CompletedCallback() {
            @Override
            public void onCompleted(Exception ex) {
                isConnected = false;
                if (ex != null) {
                    Log.e(TAG, "AsyncSocket Exception: " + ex.getMessage());
                    return;
                }
                if (onCloseCallbackListener != null) {
                    onCloseCallbackListener.onCloseCallBack();
                }
                Log.d(TAG, "---> Closed");
            }
        });

        socket.setEndCallback(new CompletedCallback() {
            @Override
            public void onCompleted(Exception ex) {
                isConnected = false;
                if (ex != null) {
                    Log.e(TAG, "AsyncSocket Exception: " + ex.getMessage());
                    return;
                }
                if (onEndCallbackListener != null) {
                    onEndCallbackListener.onEndCallBack();
                }
                Log.d(TAG, "---> Ended");
            }
        });
    }

    /**
     * write Socket with String
     */
    public boolean writeString(String str) {
        return writeString(null, str);
    }

    public boolean writeString(Context context, String str) {
        if (TextUtils.isEmpty(str)) {
            if (context != null) {
                ToastTip.show(context, "The data sent is null");
            }
            return false;
        }
        if (asyncSocket == null) {
            Log.e(TAG, "socket == null");
            return false;
        }
        if (!isConnected && context != null) {
            ToastTip.show(context, "Please try again later");
        }
        Log.d(TAG, "--------------------------------------------------------");
        Log.d(TAG, "Send: " + str);

        /*---- Prepare byte buffer from string ----*/
        short b = genRandom();
        byte[] bytes = new byte[0];
        byte[] dest = null;
        try {
            bytes = str.getBytes("UTF-8");
            int length = bytes.length;
            dest = new byte[length + 0x10];
            write((short) 0, dest, 0);
            write((short) 0, dest, 2);
            write((short) 0, dest, 4);
            write((short) 0, dest, 6);
            write((short) length, dest, 8);
            dest[10] = 0;
            dest[11] = 0;
            write(b, dest, 12);
            dest[14] = 4;
            dest[15] = 0;
            System.arraycopy(bytes, 0, dest, 0x10, length);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (dest != null) {
            Logger.d("Sending hex: " + Hex.encodeHex(dest, false));
            ByteBuffer bb = ByteBuffer.wrap(dest);
            ByteBufferList bbl = new ByteBufferList();
            bbl.add(bb);
            asyncSocket.write(bbl);
        }

        return isConnected;
    }

    /**
     * Close Socket
     */
    private void closeSocket() {
        if (asyncSocket == null) {
            Log.e(TAG, "socket == null");
            return;
        }
        Log.d(TAG, "Close Socket");
        asyncSocket.close();
        asyncSocket = null;
    }

    /**
     * After closing Socket, release resources
     */
    public void release() {
        if (mHandler != null) {
            mHandler.removeCallbacks(null);
        }
        closeSocket();
        single = null;
    }

    private static short genRandom() {
        short num;
        try {
            num = (short) new Random().nextInt();
        } catch (Exception e) {
            num = (short) new Random().nextInt();
        }
        if (num < 0) {
            num = (short) -num;
        }
        return num;
    }

    private static void write(short b, byte[] dest, int start) {
        dest[start] = (byte) (b & 0xff);
        dest[start + 1] = (byte) ((b >> 8) & 0xff);
    }

    public String read(DataInputStream inStream) {
        int available = 0;
        try {
            available = inStream.available();
            Logger.d("inStream.size = " + available);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        // Reset responseSb if we're not processing multi-part response
        if (packetSize == 0) {
            responseSb.setLength(0);
        }

        try {
            if (packetSize == 0) {
                if (readShort(inStream) == -1) {
                    return null;
                }
                if (readShort(inStream) == -1) {
                    return null;
                }
                int num = readShort(inStream);
                if (num == -1) {
                    return null;
                }
                readShort(inStream);
                if (num == -1) {
                    return null;
                }
                int num2 = readShort(inStream);
                Logger.d("num2 = " + num2);
                if (num2 == -1) {
                    return null;
                }
                if (inStream.readByte() == -1) {
                    return null;
                }
                if (inStream.readByte() == -1) {
                    return null;
                }
                if (readShort(inStream) == -1) {
                    return null;
                }
                if (inStream.readByte() == -1) {
                    return null;
                }
                if (inStream.readByte() == -1) {
                    return null;
                }

                packetSize = num2;
                available = available - 16;

                byte[] bytes = new byte[available];
                for (int i = 0; i < available; i++) {
                    int num4 = inStream.readByte();
                    if (num4 != -1) {
                        bytes[i] = (byte) num4;
                    } else {
                        return null;
                    }
                }

                String response = new String(bytes, Charset.forName("UTF-8"));
                responseSb.append(response);
            } else {
                StringBuffer sb = new StringBuffer();
                String tmp;
                while ((tmp = inStream.readLine()) != null) {
                    sb.append(tmp);
                }
                responseSb.append(sb);
            }

            Logger.d("responseSb.length = " + responseSb.length() + ", packetSize = " + packetSize);
            // Check if we receive the whole packet -> reset packet size
            if (responseSb.length() >= (packetSize - 16)) {
                packetSize = 0;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseSb.toString();
    }

    private static int readShort(DataInputStream inStream) {
        try {
            int num = inStream.readByte();
            int num2 = inStream.readByte();
            if ((num == -1) || (num2 == -1)) {
                return -1;
            }
            return (num | (num2 << 8));
        } catch (Exception e) {
            return -1;
        }
    }

}