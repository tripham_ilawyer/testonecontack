package fl.pct.com.testonecontack.model;

import org.xmlpull.v1.XmlPullParser;

/**
 * Created by Tri Pham on 12/28/2016.
 */

public abstract class ResponseEmployeeIdVisitor extends ResponseNode{
    protected ResponseEmployeeIdVisitor reiv;

    public void visitAttribute(XmlPullParser parser) {
        if (reiv != null) {
            reiv.visitAttribute(parser);
        }
    }
}
