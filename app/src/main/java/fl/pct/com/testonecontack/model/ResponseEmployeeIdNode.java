package fl.pct.com.testonecontack.model;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import fl.pct.com.testonecontack.util.XmlUtil;

/**
 * Created by Tri Pham on 12/28/2016.
 */

public class ResponseEmployeeIdNode extends ResponseEmployeeIdVisitor {
    public static final String ATTR_NAME = "employeeId";
    public TextAttribute attributeVal;

    public ResponseEmployeeIdNode() {
        attributeName = ATTR_NAME;
    }

    public ResponseEmployeeIdNode(String text) {
        attributeName = ATTR_NAME;
        this.attributeVal = new TextAttribute(text);
    }

    @Override
    public void visitAttribute(XmlPullParser parser) {
        attributeVal = new TextAttribute();
        try {
            parser.require(XmlPullParser.START_TAG, null, ATTR_NAME);
            String employeeId = XmlUtil.readText(parser);
            parser.require(XmlPullParser.END_TAG, null, ATTR_NAME);
            attributeVal.setText(employeeId);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
