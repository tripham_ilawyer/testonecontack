package fl.pct.com.testonecontack.model;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import fl.pct.com.testonecontack.util.XmlUtil;

/**
 * Created by Tri Pham on 12/29/2016.
 */

public class ResponseResultNode extends ResponseResultVisitor {
    public static final String ATTR_NAME = "result";
    public IntAttribute attributeVal;

    @Override
    public void visitAttribute(XmlPullParser parser) {
        attributeVal = new IntAttribute();
        try {
            parser.require(XmlPullParser.START_TAG, null, ATTR_NAME);
            String result = XmlUtil.readText(parser);
            parser.require(XmlPullParser.END_TAG, null, ATTR_NAME);
            attributeVal.setInt(Integer.valueOf(result));
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
}
