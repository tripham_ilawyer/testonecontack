package fl.pct.com.testonecontack.model;

import org.xmlpull.v1.XmlPullParser;

/**
 * Created by Tri Pham on 12/28/2016.
 */

public class ResponseUserNode extends ResponseUserVisitor {
    public static final String ATTR_NAME = "user";
    public ResponseCommandNode commandNode;
    public ResponseSerialIdNode serialIdNode;

    public ResponseUserNode(){
        attributeName = ATTR_NAME;
    }

    @Override
    public void visitCommand(XmlPullParser parser) {
        commandNode = new ResponseCommandNode();
        if (parser.getName().equals(ResponseCommandNode.ATTR_NAME)) {
            commandNode.visitAttribute(parser);
        }
    }

    @Override
    public void visitSerialId(XmlPullParser parser) {
        serialIdNode = new ResponseSerialIdNode();
        if (parser.getName().equals(ResponseSerialIdNode.ATTR_NAME)) {
            serialIdNode.visitAttribute(parser);
        }
    }
}
