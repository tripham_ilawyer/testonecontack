package fl.pct.com.testonecontack.model;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import fl.pct.com.testonecontack.util.XmlUtil;

/**
 * Created by Tri Pham on 12/28/2016.
 */

public class ResponseBodyNode extends ResponseBodyVisitor {
    public static final String ATTR_NAME = "body";
    public ResponseEmployeesNode employeesNode;
    public ResponseResultNode resultNode;

    public ResponseBodyNode(){
        attributeName = ATTR_NAME;
    }

    @Override
    public void visitResult(XmlPullParser parser) {
        resultNode = new ResponseResultNode();
        if (parser.getName().equals(ResponseResultNode.ATTR_NAME)) {
            resultNode.visitAttribute(parser);
        }
    }

    @Override
    public void visitEmployees(XmlPullParser parser) {
        employeesNode = new ResponseEmployeesNode();
        try {
            while (parser.next() != XmlPullParser.END_TAG || !parser.getName().equals(ResponseEmployeesNode.ATTR_NAME)) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                String name = parser.getName();

                // TODO: Add parsing other attribute within "<employees></employees>" here orderly

                // Starts by looking for the entry tag
                if (name.equals(ResponseEmployeeIdNode.ATTR_NAME)) {
                    employeesNode.visitEmployeeId(parser);
                } else {
                    XmlUtil.skip(parser);
                }
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
