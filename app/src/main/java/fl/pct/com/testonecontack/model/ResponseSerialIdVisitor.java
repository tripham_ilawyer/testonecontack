package fl.pct.com.testonecontack.model;

import org.xmlpull.v1.XmlPullParser;

/**
 * Created by Tri Pham on 12/29/2016.
 */

public class ResponseSerialIdVisitor extends ResponseNode {
    protected ResponseSerialIdVisitor rsiv;

    public void visitAttribute(XmlPullParser parser) {
        if (rsiv != null) {
            rsiv.visitAttribute(parser);
        }
    }
}
