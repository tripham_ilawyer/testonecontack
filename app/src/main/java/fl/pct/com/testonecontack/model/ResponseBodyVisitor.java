package fl.pct.com.testonecontack.model;

import org.xmlpull.v1.XmlPullParser;

/**
 * Created by Tri Pham on 12/28/2016.
 */

public abstract class ResponseBodyVisitor extends ResponseNode{
    protected ResponseBodyVisitor rbv;

    public void visitResult(XmlPullParser parser) {
        if (rbv != null) {
            rbv.visitResult(parser);
        }
    }
    public void visitEmployees(XmlPullParser parser) {
        if (rbv != null) {
            rbv.visitEmployees(parser);
        }
    }
}
