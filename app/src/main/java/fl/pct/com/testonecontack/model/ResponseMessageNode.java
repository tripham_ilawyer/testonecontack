package fl.pct.com.testonecontack.model;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import fl.pct.com.testonecontack.util.XmlUtil;

/**
 * Created by Tri Pham on 12/29/2016.
 */

public class ResponseMessageNode extends ResponseMessageVisitor {
    public static final String ATTR_NAME = "message";
    public ResponseUserNode userNode;
    public ResponseBodyNode bodyNode;

    public ResponseMessageNode() {
        attributeName = ATTR_NAME;
    }

    @Override
    public void visitUser(XmlPullParser parser) {
        userNode = new ResponseUserNode();
        try {
            while (parser.next() != XmlPullParser.END_TAG || !parser.getName().equals(ResponseUserNode.ATTR_NAME)) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                String name = parser.getName();

                // TODO: Add parsing other attribute within "<user></user>" here orderly

                // Starts by looking for the entry tag
                if (name.equals(ResponseCommandNode.ATTR_NAME)) {
                    userNode.visitCommand(parser);
                } else if (name.equals(ResponseSerialIdNode.ATTR_NAME)) {
                    userNode.visitSerialId(parser);
                } else {
                    XmlUtil.skip(parser);
                }
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void visitBody(XmlPullParser parser) {
        bodyNode = new ResponseBodyNode();
        try {
            while (parser.next() != XmlPullParser.END_TAG || !parser.getName().equals(ResponseBodyNode.ATTR_NAME)) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                String name = parser.getName();

                // TODO: Add parsing other attribute within "<body></body>" here orderly

                // Starts by looking for the entry tag
                if (name.equals(ResponseResultNode.ATTR_NAME)) {
                    bodyNode.visitResult(parser);
                } else if (name.equals(ResponseEmployeesNode.ATTR_NAME)) {
                    bodyNode.visitEmployees(parser);
                } else {
                    XmlUtil.skip(parser);
                }
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
