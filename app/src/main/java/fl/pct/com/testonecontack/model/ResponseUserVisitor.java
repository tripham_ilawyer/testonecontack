package fl.pct.com.testonecontack.model;

import org.xmlpull.v1.XmlPullParser;

/**
 * Created by Tri Pham on 12/28/2016.
 */

public abstract class ResponseUserVisitor extends ResponseNode {
    protected ResponseUserVisitor ruv;

    public void visitCommand(XmlPullParser parser) {
        if (ruv != null) {
            ruv.visitCommand(parser);
        }
    }

    public void visitSerialId(XmlPullParser parser) {
        if (ruv != null) {
            ruv.visitSerialId(parser);
        }
    }
}
